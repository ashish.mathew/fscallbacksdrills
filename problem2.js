const fs = require("fs");
const path = require("path");

module.exports = () => {
  try {
    fs.readFile(path.join(__dirname, "lipsum.txt"), "utf-8", (err, data) => {
      fs.writeFile(
        path.join(__dirname, "lipsumUpper.txt"),
        data.toUpperCase(),
        () => {
          fs.appendFile(
            path.join(__dirname, "filename.txt"),
            "lipsumUpper.txt",
            () => {
              fs.readFile(
                path.join(__dirname, "lipsumUpper.txt"),
                "utf-8",
                (err, data) => {
                  fs.writeFile(
                    path.join(__dirname, "lipsumSplit.txt"),
                    JSON.stringify(data.toLowerCase().split(".")),
                    () => {
                      fs.appendFile(
                        path.join(__dirname, "filename.txt"),
                        "\nlipsumSplit.txt",
                        () => {
                          fs.readFile(
                            path.join(__dirname, "lipsumSplit.txt"),
                            "utf-8",
                            (err, data) => {
                              fs.writeFile(
                                path.join(__dirname, "lipssumSort.txt"),
                                JSON.stringify(JSON.parse(data).sort()),
                                () => {
                                  fs.appendFile(
                                    path.join(__dirname, "filename.txt"),
                                    "\nlipsumSort.txt",
                                    () => {
                                      fs.readFile(
                                        path.join(__dirname, "filename.txt"),
                                        "utf-8",
                                        (err, data) => {
                                          data.split("\n").forEach((data) => {
                                            fs.unlink(
                                              path.join(__dirname, `${data}`),
                                              () => {
                                                console.log(`delete ${data}`);
                                              }
                                            );
                                          });
                                        }
                                      );
                                    }
                                  );
                                }
                              );
                            }
                          );
                        }
                      );
                    }
                  );
                }
              );
            }
          );
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};
