const fs = require("fs");
const path = require("path");

module.exports = () => {
  try {
    fs.mkdir(path.join("..", "randomJson"), () => {
      console.log("created random");
      fs.writeFile(
        path.join("..", "randomJson", "things1.json"),
        JSON.stringify("helo"),
        () => {
          console.log("created things1");
          fs.writeFile(
            path.join("..", "randomJson", "things2.json"),
            JSON.stringify("hey"),
            () => {
              console.log("created things2");
              fs.unlink(path.join("..", "randomJson", "things2.json"), () => {
                console.log("deleted things2");
                fs.unlink(path.join("..", "randomJson", "things1.json"), () => {
                  console.log("deleted things1");
                  fs.rmdir(path.join("..", "randomJson"), () => {
                    console.log("deleted dir");
                  });
                });
              });
            }
          );
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};
